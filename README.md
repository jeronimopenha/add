# ADD - Project

ADD - Accelerator Design and Deploy tool repository.

The tool documentation can be found at:

https://www.overleaf.com/read/zhkgsmnbmjrs (under construction)

The documentation for the tool code can be found in the ADD_Accelerator_Design_and_Deploy / docs folder.

Contact: jeronimopenha@gmail.com